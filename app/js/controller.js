var app = angular.module("firebaseApp", ["firebase", "uiGmapgoogle-maps", "angular.filter"])

.filter("format", function(){
	return function(a){		
		// console.log(a);

		var arr = [];

		angular.forEach(a, function(value, key){
				// console.log(value);

			if (undefined !== value[2] && undefined !== value[8]) {
			
				var 
					newValue = value[2].match(/(.+),/),
					year = parseInt(value[1]);

				console.log(value[8]);

				value[8] = value[8].split("_N_");


				if ( 
					newValue && null !== newValue.length &&
					value[8] && null !== value[8].length
					) {
					value[2] = newValue[1];
					arr.push({
						id: key,
						city: value[0],
						year: year,
						country: value[2],
						text: value[9],
						latitude: value[8][0],
						longitude: value[8][1],
						century: (year - year % 100) / 100
					});
				}
			};
		});

		return arr;
	}
})


app.controller("firebaseCtrl", [ "$scope", "$filter", "$firebaseObject", function($scope, $filter, $firebaseObject){

	var ref = new Firebase("https://meteorit.firebaseio.com");

	$scope.coords = $firebaseObject(ref);

	// ref.authWithCustomToken("H14T8HIJvfsv9n9pdhsNq3mrrW4oveGSznHIjf08", function(error, authData) {
	//   if (error) {
	//     console.log("Login Failed!", error);
	//   } else {
	//     console.log("Login Succeeded!", authData);
	//   }
	// });

	$scope.showMap = false;

	$scope.coords.$loaded(function(){
		$scope.meteorits = $filter("format")( $scope.coords );
		// $scope.meteorits = $filter("formatCoords")( $scope.coords );
		// $scope.formatYear = $filter("formatYear")( $scope.coords );
		$scope.showMap = true;
		console.log($scope.formatCountry);
	});

    $scope.map = {
		center: {
			// latitude: 34.3294399,
			// longitude: 10.7780032
			latitude: 56.574356,
			longitude: 87.067066
		},
		zoom: 3,
		bounds: {}
    };

    $scope.options = {
      	scrollwheel: true
    };

	// $scope.$watch("coords", function( newValue ){
		
	// 	$scope.coordsFormated = $filter("formatCoords")( $scope.coords );

	// });

	// $scope.$watch("coordsFormated", function( newValue ){
		
	// 	// console.log($scope.coordsFormated);

	// });

}])